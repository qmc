/*
 * Copyright (c) 2009 Mauro Iazzi
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "qmc.hpp"
#include "config.hpp"

using namespace std;

int main (int argc, char **argv) {
    Config conf (argc, argv);
    SpinChain init = conf.init;
    double alpha = conf.alpha;
    int nspins = conf.nspins;
    int niter = conf.niter;
    std::ofstream &out = conf.out;
    bool print_state = conf.print_state;
    Energy en(conf.J, conf.D);
    int L = conf.L;

    int index = 0; // first of reptile
    Walker start (VarProb(alpha), init, en);
    vector<Walker> reptile(L, start);

    double a = 0.0; // acceptance ratio
    bool accepted = true; // first value is always accepted
    int d = rand()<(RAND_MAX/2)?1:-1;
    int first = 0;
    int last = L;
    double e = 0.0;
    double w = 0.0;
    std::string state_str = init.toString();
    for (int i=0;i<L-1;i++) {
        reptile[i].copyOn(&reptile[i+1]);
        reptile[i+1].step();
    }
    for (int i=1;i<=niter;i++) {
        d = rand()<(RAND_MAX/2)?1:-1;
        first = (L+index+d)%L;
        last = (L+index-d)%L;
        a = reptile[last].weight()/reptile[first].weight();
        accepted = a>(rand()/(RAND_MAX+1.0));
        if (!accepted) {
            // e still contains the correct value
        } else {
            //e = en.compute(mc.state(), mc.helper(), mc.probability());
            reptile[last].copyOn(&reptile[index]);
            //cerr << "accepted d=" << d << " " << reptile[index].step() << endl;
            reptile[index].step();
            e = ( reptile[index].localEnergy() + reptile[first].localEnergy() ) / 2.0;
            if (print_state) state_str = reptile[index].state().toString();
            //w = std::exp(start.probability().compute(reptile[(d==1?first+1:index)%L].state()));
            //w = w*w;
            w = 1.0;
            for (int i=0;i<L;i++) {
                w *= (reptile[i].coeff());
            }
        }
	if (conf.M) {
		e = reptile[(L+(index-first)/2)%L].state().staggeredMagnetization();
	}
        if (out.is_open()) {
            if (print_state) {
                out << state_str << " " << (e/nspins) << " " << w << "\n";
            } else {
                out << "* " << (e/nspins) << " " << w << "\n";
            }
        }
        if (accepted) index = last;
    }
    if (out.is_open()) {
        out.close();
    }
    return 0;
}

