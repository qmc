#!/usr/bin/lua

local r, s, e, d = ...

r = tonumber(r or 666)
s = tonumber(s or 20)
e = tonumber(e or 200)
d = tonumber(d or 10)

local t = 'fmc'
for l = s, e, d do
	l = string.format('%.3i', l)
	print('==== '..t..' len = '..l..' ====')
	os.execute('time ./'..t..' -M -s -r '..tostring(r)..' -l 200 -n '..l..' -i 1000000 -a 0.22 -o raw-L-'..l..'.'..t)
	print()
	os.execute('./shuffle.lua raw-L-'..l..'.'..t)
	print()
end

os.execute('./analyze -w 3 -s 000 -l 1000 *.fmc.shf | tee mag.'..tostring(r))
os.execute('./analyze -w 3 -s 000 -l 1000 *.fmc.shf -q | tee sq.'..tostring(r))

os.execute('echo \'plot "mag.'..tostring(r)..'" using (1/$1):3:4 with yerrorlines\' >> plotmag.gp')
os.execute('echo \'plot "sq.'..tostring(r)..'" using (1/$1):3:4 with yerrorlines\' >> plotsq.gp')

