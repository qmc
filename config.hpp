/*
 * Copyright (c) 2009 Mauro Iazzi
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __CONFIG_HPP
#define __CONFIG_HPP

#include "qmc.hpp"

void print_help () {
}

struct Config {
    int seed;
    bool print_state;
    std::ofstream out;
    int nspins;
    int niter;
    int L;
    bool M;
    double alpha;
    SpinChain init;
    double J;
    double D;
    void _default () {
        print_state = false;
        seed = time(NULL);
        alpha = 0.24;
        nspins = 100;
        niter = int(1e6);
        L = 10;
	J = 1.0;
        D = 0.8;
        M = false;
        init = SpinChain::neel(nspins);
    }
    Config () {
        _default();
    }
    Config (int argc, char **argv) {
        _default();
        std::string filename;
        for (int i=1;i<argc;i++) {
            if (argv[i][0]!='-') {
                print_help();
                exit(1);
            }
            switch (argv[i][1]) {
                case 's':
                    print_state = true;
                    break;
                case 'o':
                    filename = argv[++i];
                    break;
                case 'r':
                    srand(atoi(argv[++i]));
                    break;
                case 'i':
                    niter = atoi(argv[++i]);
                    break;
                case 'M':
                    M = true;
                    break;
                case 'l':
                    L = atoi(argv[++i]);
                    break;
                case 'j':
                case 'J':
                    J = atof(argv[++i]);
                    break;
                case 'd':
                case 'D':
                    D = atof(argv[++i]);
                    break;
                case 'a':
                    alpha = atof(argv[++i]);
                    break;
                case 'n':
                    nspins = atoi(argv[++i]);
                    break;
                case 'h':
                case 'H':
                    print_help();
                    exit(0);
                    break;
                default:
                    print_help();
                    exit(1);
                    break;
            }
        }
        if (filename=="<>") {
        } else if (filename!="") {
            out.open(filename.c_str(), std::ofstream::out);
        }
        init = SpinChain::neel(nspins);
        if (out.is_open()) {
            out.precision(10);
            out << "#!prepend " << alpha << "\n";
            out << "#!prepend " << nspins << "\n";
            out << "# alpha = " << alpha << "\n";
            out << "# number of spins = " << nspins << "\n";
            out << "# J = " << J << "\n";
            out << "# Delta = " << D << "\n";
            out << "# value = " << (M?"magnetization":"energy") << "\n";
        }
    }
};

#endif // __CONFIG_HPP

