/*
 * Copyright (c) 2009 Mauro Iazzi
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstdlib>

using namespace std;

struct Result {
    std::string prepend;
    std::vector<std::string> states;
    std::vector<double> energies;
    std::vector< std::vector<double> > weights;
    std::vector<int> lengths;
};

void createarrays (std::string filename, Result& res, const int each, double c, bool use_log = true) {
    const int len = *(res.lengths.end());
    int n = 0;
    double *v = new double[len];
    int i = 0;
    for (int j=0;j<len;j++) v[i] = 0;
    std::string line_string = "";
    std::string state = "";
    std::ifstream f(filename.c_str(), std::fstream::in);
    while (!f.eof()) {
        double energy = 0.0;
        double weight = 0.0;
        getline(f, line_string);
        std::stringstream line(line_string);
        if (line_string[0]=='#') {
            res.prepend.append(line_string);
        } else if (line_string.size()!=0) {
            double nw = use_log?0.0:1.0;
            line >> state >> energy >> weight;
            res.states.push_back(state);
            res.energies.push_back(energy);
            if (use_log) {
                weight = std::log(weight) - c;
            } else {
                weight = weight * std::exp(-c);
            }
            v[i] = weight;
            int which_l = 0;
            for (int j=0;j<len;j++) {
                if (use_log) {
                    nw += v[(len+i-j)%len];
                } else {
                    nw *= v[(len+i-j)%len];
                }
                if (j==res.lengths[which_l]) {
                    res.weights[which_l].push_back(use_log?std::exp(nw):nw);
                    which_l++;
                }
            }
            i = (i+1) % len;
        }
    }
    f.close();
    delete v;
}

void analyze_data_file (std::string filename, std::ostream& out, const int len, double c, bool use_log = true) {
    int n = 0;
    double *v = new double[len];
    int i = 0;
    for (int j=0;j<len;j++) v[i] = 0;
    std::string line_string = "";
    std::string state = "";
    std::ifstream f(filename.c_str(), std::fstream::in);
    while (!f.eof()) {
        double energy = 0.0;
        double weight = 0.0;
        getline(f, line_string);
        std::stringstream line(line_string);
        if (line_string[0]=='#') {
            out << line_string << "\n";
        } else if (line_string.size()!=0) {
            double nw = use_log?0.0:1.0;
            line >> state >> energy >> weight;
            if (use_log) {
                weight = std::log(weight) - c;
            } else {
                weight = weight * std::exp(-c);
            }
            out << state << " " << energy;
            v[i] = weight;
            for (int j=0;j<len;j++) {
                if (use_log) {
                    nw += v[(len+i-j)%len];
                } else {
                    nw *= v[(len+i-j)%len];
                }
            }
            out << " " << (use_log?std::exp(nw):nw);
            out << "\n";
            i = (i+1) % len;
        }
    }
    f.close();
    delete v;
}

int main (int argc, char **argv) {
    string len_s = "1000";
    int len = 1000;
    double corr = 0.0;
    vector<string> files;
    for (int i=1;i<argc;i++) {
        if (argv[i][0]=='-') {
            switch (argv[i][1]) {
                case 'c':
                    corr = atof(argv[++i]);
                    break;
                case 'l':
                    len = atoi(argv[++i]);
                    len_s = argv[i];
                    break;
            }
        } else {
            files.push_back(argv[i]);
        }
    }
    for (vector<string>::iterator iter = files.begin();iter != files.end();iter++) {
        string ofn("l-");
        ofn.append(len_s);
        ofn.append("-");
	ofn.append(*iter);
        fstream o(ofn.c_str(), fstream::out);
        o.precision(10);
        o << "#!prepend " << len_s << "\n";
        analyze_data_file(*iter, o, len, corr);
        o.close();
    }
    return 0;
}

