/*
 * Copyright (c) 2009 Mauro Iazzi
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstdlib>

using namespace std;

std::pair<double, double> compute_stats (const std::vector<double> &d) {
    double avg = 0.0, var = 0.0;
    const int N = d.size();
    for (int i=0;i<N;i++) {
        //cerr << d[i] << " ";
        avg += d[i];
        var += d[i]*d[i];
    }
    avg /= N;
    var /= (N);
    //cerr.precision(10);
    //cerr << d[0] << " " << d[49] << " " << d[311] << " " << d[N-1] << endl;
    //cerr << avg*avg << " " << var << endl;
    var -= avg*avg;
    var = sqrt(var);
    //cerr << "=> " << avg << " +- " << var << endl;
    return std::pair<double, double>(avg, var);
}

std::string analyze_data_file (std::string filename, const int skip, const int len, int w, bool sq = false) {
    std::vector<double> sums;
    std::string ret = "";

    int n = -skip-1;
    double norm = 0.0;
    std::string line_string = "";
    std::string state = "";
    std::ifstream f(filename.c_str(), std::fstream::in);
    while (!f.eof()) {
        double energy = 0.0;
        double weight = 0.0;
        getline(f, line_string);
        std::stringstream line(line_string);
        if (line_string[0]=='#') {
            if (line_string[1]=='!') {
                if (line_string.substr(2, 8)=="prepend ") {
                    ret.insert(0, " ");
                    ret.insert(0, line_string.substr(10, line_string.size()));
                } else {
                }
            }
        } else {
            n++;
            if (n>=0) {
                line >> state >> energy;
		if (sq) energy = energy*energy;
                weight = 1.0;
                for (int j=0;j<w;j++) {
                    line >> weight;
                }
                norm += (weight);
                if (n%len==0) {
                    if (n>0) sums[sums.size()-1] /= norm;
                    sums.push_back(0.0);
                    norm = 0.0;
                }
                sums[n/len] += energy*(weight);
            }
        }
    }
    f.close();
    sums.pop_back(); // last one may be incomplete.
    // time to do some thing with the sums (now they are averages)
    std::pair<double, double> s = compute_stats(sums);
    std::stringstream rs;
    rs << s.first << " " << s.second;
    ret.append(rs.str());
    return ret;
}

#define NEXTOPT(a, n) ( (a[i][2]=='\0')?(a[++i]):&(a[i][2]) )

int main (int argc, char **argv) {
    int skip = 0;
    int len = 1000;
    int w_col = -1;
    bool square = false;
    vector<string> files;
    for (int i=1;i<argc;i++) {
        if (argv[i][0]=='-') {
            switch (argv[i][1]) {
                case 'w':
                    w_col = atoi(NEXTOPT(argv, i));
                    break;
                case 's':
                    skip = atoi(NEXTOPT(argv, i));
                    break;
                case 'q':
                    square = true;
                    break;
                case 'l':
                    len = atoi(NEXTOPT(argv, i));
                    break;
            }
        } else {
            files.push_back(argv[i]);
        }
    }
    for (vector<string>::iterator iter = files.begin();iter != files.end();iter++) {
        cout << analyze_data_file(*iter, skip, len, w_col, square) << endl;
    }
    return 0;
}

