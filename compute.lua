#!/usr/bin/lua

local fn = ...
local f = assert(io.open(fn))

local l = f:read()
local patt = '^(#?)(%S+)'..('%s*(%S*)'):rep(2)

local r = setmetatable({}, {__index=table})
local transform = function(l, c, s, e, w)
	if c=='#' then
		r:insert(l)
	else
		r:insert('* '..e)
	end
	return c, s
end

while l do
	transform(l, l:match(patt))
	l = f:read()
end

f:close()

