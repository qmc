#!/usr/bin/lua

local f, s, e, d, c = ...

if f=='-h' or f=='--help' then
	io.stderr:write([[
usage: ]]..arg[0]..[[ <f> <s> <e> <d> <c>
	<f> file on which to compute
	<s> (default 10) starting value of alpha
	<e> (default 500) final value of alpha
	<d> (default 10) stepping value of alpha
	<c> (default 3.7) correction value
]])
	return
end

s = (s or 10)
e = (e or 500)
d = (d or 10)
c = (c or 3.0)

local digits = #tostring(s)
if digits < #tostring(e) then
	digits = #tostring(e)
end

s, e, d, c = tonumber(s), tonumber(e), tonumber(d), tonumber(c)

for l = s, e, d do
	l = string.format('%.'..tostring(digits)..'i', l)
	print('==== length = '..l..' ====')
	os.execute('time ./compute_weights '..f..' -l '..l..' -c '..c)
	print()
end

