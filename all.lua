#!/usr/bin/lua

local n, i, s, e, d = ...

if n=='-h' or n=='--help' then
	io.stderr:write([[
usage: ]]..arg[0]..[[ <n> <i> <s> <e> <d>
	<n> (default 20) Number of spins
	<i> (default 1000000) Iterations for each value of alpha
	<s> (default 0.1) starting value of alpha
	<e> (default 0.4) final value of alpha
	<d> (default 0.01) stepping value of alpha
]])
	return
end

n = tostring(tonumber(n) or 20)
i = tostring(tonumber(i) or 1000000)
s = tonumber(s or 0.22)
e = tonumber(e or 0.22)
d = tonumber(d or 0.01)

local t = 'rmc'
for a = s, e, d do
	a = string.format('%.3f', a)
	print('==== '..t..' alpha = '..a..' ====')
	os.execute('time ./'..t..' -M -l 100 -n '..n..' -i '..i..' -a '..a..' -o raw-'..a..'.'..t)
	print()
end

