CFLAGS=-O3 -march=core2
CXXFLAGS=-O3 -march=core2
LDFLAGS=-lm -ldl
SYSTEM=linux
LUA=lua-5.1.4

targets=analyze compute_weights dmc vmc rmc fmc
objects=$(targets:%=%.o)
deps=$(targets:%=.%.dep)

all: $(targets)

debug:
	$(MAKE) CXXFLAGS="-g -ggdb" CFLAGS="-g -ggdb"

$(LUA).tar.gz:
	wget http://www.lua.org/ftp/$(LUA).tar.gz

$(LUA)/src/liblua.a: $(LUA).tar.gz
	tar -xvzf $(LUA).tar.gz
	make -C $(LUA)/src liblua.a MYCFLAGS="-DLUA_USE_LINUX $(CFLAGS)"

$(objects): %.o: Makefile .%.dep

$(deps): .%.dep: %.cpp
	$(CXX) -MM $< > $@

$(targets): %: %.o $(LUA)/src/liblua.a
	$(CXX) $(LDFLAGS) -I$(LUA)/src $@.o $(LUA)/src/liblua.a -o $@

clean:
	make -C $(LUA) clean
	rm -f $(targets) $(objects)

.PHONY: clean default all

include $(deps)


