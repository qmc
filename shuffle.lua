#!/usr/bin/lua

local fn = ...

local it = setmetatable({}, { __index=table })
--local ot = setmetatable({}, { __index=table })

local f = assert(io.open(fn, 'r'))
local l = f:read()
while l do
	it:insert(l)
	l = f:read()
end
f:close()

print('read\t '..tostring(#it)..' lines')

for i = 1, #it do
	local j = math.random(i, #it)
	it[i], it[j] = it[j], it[i]
	io.write('shuffled '..tostring(i)..' lines\r')
	io.flush()
end
print()

local f = assert(io.open(fn..'.shf', 'w'))
for i, l in ipairs(it) do
	io.write('written\t '..tostring(i)..' lines\r')
	io.flush()
	f:write(l..'\n')
end
f:close()

print('written\t '..tostring(#it)..' lines')

