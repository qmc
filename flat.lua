#!/usr/bin/lua

-- checks the distribution of numbers in a data file

local f = io.open(... or 'test')

local t = {}
local l = f:read()
while l do
	t[tonumber(l)] = (t[tonumber(l)] or 0) + 1
	l = f:read()
end

f:close()

for i, n in ipairs(t) do
	print(i, n)
end

